package com.example.parseimage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.util.ArrayList


class ParseAdapter(private val parseItems: ArrayList<ParseItem>, private val context: Context) :
    RecyclerView.Adapter<ParseAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParseAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ParseAdapter.ViewHolder, position: Int) {
        val parseItem = parseItems[position]
        holder.sizeView.setText(parseItem.imgSize)
        holder.urlView.setText(parseItem.absUrl)
        Picasso.get().load(parseItem.herf).into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return parseItems.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        override fun onClick(p0: View?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        internal var imageView: ImageView = view.findViewById(R.id.imageView)
        internal var sizeView: TextView = view.findViewById(R.id.sizeImage)
        internal var urlView: TextView = view.findViewById(R.id.urlImage)


    }
}
