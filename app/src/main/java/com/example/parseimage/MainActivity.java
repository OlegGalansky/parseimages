package com.example.parseimage;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText;
    private Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);

        button = findViewById(R.id.button);
        button.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
//        try {
            String url = editText.getText().toString();
//            String url = "https://ru.wikipedia.org";

            Intent intent = new Intent(this, ViewActivity.class);
//            Document doc = Jsoup.connect(url).get();
//            Elements data = doc.getElementsByTag("img");

//            if (data.isEmpty()){
//                Toast.makeText(this,"\n" +
//                        "No pictures on the site", Toast.LENGTH_SHORT).show();
//            }else{
              intent.putExtra("url",url);
              startActivity(intent);
//            }
//        }catch (IOException e) {
//            Toast.makeText(this,"\n" +
//                    "Not possible to connect", Toast.LENGTH_SHORT).show();
//            Log.d("e", e.toString());
//        }

    }
}

